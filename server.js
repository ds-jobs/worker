const amqp = require('amqplib/callback_api');

const config = require('./env.json')

const queueSync = 'queueSync';
const queueAsync = 'queueAsync';

amqp.connect(config.RABBITMQ_AMQP, (error0, connection) => {
    if (error0) {
        throw error0;
    }
    connection.createChannel((error1, channel) => {
        channel.assertQueue(queueSync, {durable: false});
        channel.assertQueue(queueAsync, {durable: false});

        console.log(' [x] Awaiting requests');

        channel.consume(queueAsync, (msg) => {
            const msgContent = msg.content;
            console.log(" [.] Received message in %s: %s", queueSync, msgContent);
            // DO SOMETHING
            channel.ack(msg);
        });

        channel.consume(queueSync, (msg) => {
            const msgContent = msg.content;
            console.log(" [.] Received message in %s: %s", queueAsync, msgContent);
            for (let i = 1; i <= 5; i++) {
                channel.sendToQueue(msg.properties.replyTo,
                    Buffer.from(`{hotels: [{response ${i}]}`), {
                        correlationId: msg.properties.correlationId
                    });
                console.log(" [.] sent response %s", i);
            }
            channel.ack(msg);
        });

    })
})